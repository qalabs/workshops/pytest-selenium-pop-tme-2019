from abc import abstractmethod


class ContactsAppPage:
    """This is base class page for all Contacts App pages"""

    def __init__(self, driver) -> None:
        """
        Initialize the page by passing driver instance

        :type driver: selenium.webdriver.remote.webdriver.WebDriver
        """
        self.driver = driver

    @abstractmethod
    def is_at(self):
        return False

    def get_page_title(self):
        return self.driver.find_element_by_css_selector("nav div.v-toolbar__title").text

    def open_about_dialog(self):
        about_button = self.driver.find_element_by_css_selector("#app-nav > div.v-toolbar__content button")
        about_button.click()
        from pages.about_dialog import AboutDialog
        return AboutDialog(self.driver)

    def logout(self):
        logout_button = self.driver.find_element_by_css_selector("#app-nav > div.v-toolbar__content > button")
        logout_button.click()
        from pages.home_page import HomePage
        return HomePage(self.driver)
