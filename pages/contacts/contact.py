class Contact(object):
    def __init__(self, name=None, email=None) -> None:
        self.name = name
        self.email = email

    def __eq__(self, o: object) -> bool:
        return self.name == o.name and self.email == o.email
