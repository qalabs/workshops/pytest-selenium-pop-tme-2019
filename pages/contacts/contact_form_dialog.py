from pages.dialog import Dialog


class ContactFormDialog(Dialog):
    def __init__(self, driver) -> None:
        super().__init__(driver)

    def set_name(self, value):
        self._set_input_value("input[name=contact-name]", value)

    def set_email(self, value):
        self._set_input_value("input[name=contact-email]", value)

    def set_contact(self, contact):
        if contact.name:
            self.set_name(contact.name)
        if contact.email:
            self.set_email(contact.email)

    def _set_input_value(self, css_selector, value):
        input = self.driver.find_element_by_css_selector(css_selector)
        input.clear()
        input.send_keys(value)

    def submit(self):
        self.driver.find_element_by_css_selector("button[type=submit]").click()
        from pages.contacts.contacts_page import ContactsPage
        return ContactsPage(self.driver)
