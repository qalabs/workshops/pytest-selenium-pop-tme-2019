from pages.contacts.contact import Contact
from pages.contacts_app_page import ContactsAppPage


class ContactsPage(ContactsAppPage):
    def __init__(self, driver) -> None:
        super().__init__(driver)

    def is_at(self):
        return self.driver.current_url.endswith("/#/")

    def open_contact_form_dialog(self):
        self.driver.find_element_by_xpath("//button//*[contains(text(), 'New Contact')]").click()
        from pages.contacts.contact_form_dialog import ContactFormDialog
        return ContactFormDialog(self.driver)

    def get_visible_contacts(self):
        contacts = []
        rows = self.driver.find_elements_by_css_selector("#contacts-list table tbody tr")
        for table_row in rows:
            contact = Contact()
            cells = table_row.find_elements_by_css_selector("td span[data-property]")
            for cell in cells:
                data_property = cell.get_attribute("data-property")
                if data_property == 'contact-name':
                    contact.name = cell.text.strip() or None
                if data_property == 'contact-email':
                    contact.email = cell.text.strip() or None
            contacts.append(contact)
        return contacts
