from pages.contacts_app_page import ContactsAppPage


class HomePage(ContactsAppPage):
    def __init__(self, driver) -> None:
        super().__init__(driver)

    def is_at(self):
        return self.driver.current_url.endswith("/#/home")

    def get_page_title(self):
        return self.driver.find_element_by_css_selector("nav div.v-toolbar__title").text

    def get_heading(self):
        return self.driver.find_element_by_css_selector("#home h1").text

    def get_sub_heading(self):
        return self.driver.find_element_by_css_selector("#home p").text

    def navigate_to_login_page(self):
        self.driver.find_element_by_xpath("//*[@id='home']/nav//button/div[contains(text(), 'Log in')]").click()
        from pages.login.login_page import LoginPage
        return LoginPage(self.driver)

    def navigate_to_signup_page(self):
        self.driver.find_element_by_xpath("//*[@id='home']/nav//button/div[contains(text(), 'Signup')]").click()
        from pages.signup.signup_page import SignupPage
        return SignupPage(self.driver)

    def navigate_to_contact_us_page(self):
        self.driver.find_element_by_xpath("//*[@id='home']/nav//button/div[contains(text(), 'Contact Us')]").click()
        from pages.contact_us_page import ContactUsPage
        return ContactUsPage(self.driver)

    def open_login_dialog(self):
        self.driver.find_element_by_xpath(
            "//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]").click()
        from pages.login.login_dialog import LoginDialog
        return LoginDialog(self.driver)

    def open_signup_dialog(self):
        self.driver.find_element_by_xpath(
            "//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Signup')]").click()
        from pages.signup.signup_dialog import SignupDialog
        return SignupDialog(self.driver)
