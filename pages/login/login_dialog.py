from pages.dialog import Dialog
from pages.login.login_actions import LoginActions


class LoginDialog(Dialog, LoginActions):
    def __init__(self, driver) -> None:
        super().__init__(driver)
