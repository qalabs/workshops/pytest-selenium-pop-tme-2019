from selenium.webdriver.support.wait import WebDriverWait


class LoginActions:
    def __init__(self, driver) -> None:
        """
        :type driver: selenium.webdriver.remote.webdriver.WebDriver
        """
        self.driver = driver

    def login(self, username, password):
        driver = self.driver
        driver.find_element_by_id("username").send_keys(username)
        driver.find_element_by_id("password").send_keys(password)
        driver.find_element_by_css_selector("button[type='submit']").click()
        from pages.contacts.contacts_page import ContactsPage
        return ContactsPage(driver)

    def login_expecting_form_errors(self, username, password):
        driver = self.driver
        driver.find_element_by_id("username").send_keys(username)
        driver.find_element_by_id("password").send_keys(password)
        driver.find_element_by_css_selector("button[type='submit']").click()
        return WebDriverWait(driver, 2).until(lambda _: self._non_empty_texts("#login-form .v-messages__message"))

    def _non_empty_texts(self, css_locator):
        elements = self.driver.find_elements_by_css_selector(css_locator)
        texts = [item.text for item in elements]
        return texts if all(texts) else False
