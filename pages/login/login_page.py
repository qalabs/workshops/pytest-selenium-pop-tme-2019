from pages.contacts_app_page import ContactsAppPage
from pages.login.login_actions import LoginActions


class LoginPage(ContactsAppPage, LoginActions):
    def __init__(self, driver) -> None:
        super().__init__(driver)

    def is_at(self):
        return self.driver.current_url.endswith("/#/login")

    def cancel(self):
        self.driver.find_element_by_xpath("//button/div[contains(text(), 'Cancel')]").click()
        from pages.home_page import HomePage
        return HomePage(self.driver)
