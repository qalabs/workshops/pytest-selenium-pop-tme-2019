from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from pages.contacts_app_page import ContactsAppPage


class Dialog(ContactsAppPage):
    def __init__(self, driver) -> None:
        super().__init__(driver)
        self.wait = WebDriverWait(driver, 5)
        dialog_locator = (By.CSS_SELECTOR, ".v-dialog.v-dialog--active .v-card")
        self.dialog = self.wait.until(ec.visibility_of_element_located(dialog_locator))

    def is_at(self):
        return self.dialog.is_displayed()

    def get_dialog_title(self):
        return self.dialog.find_element_by_css_selector(".v-card__title").text

    def close(self):
        self.dialog.find_element_by_css_selector(".v-card__actions > button.primary--text").click()
