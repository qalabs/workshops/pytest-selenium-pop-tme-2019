from pages.contacts_app_page import ContactsAppPage


class SignupPage(ContactsAppPage):
    def __init__(self, driver) -> None:
        super().__init__(driver)

    def is_at(self):
        return self.driver.current_url.endswith("/#/register")

    def cancel(self):
        self.driver.find_element_by_xpath("//button/div[contains(text(), 'Cancel')]").click()
        from pages.home_page import HomePage
        return HomePage(self.driver)
