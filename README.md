# Page Object Pattern with Python

## Install the project

- Clone the repository and change to directory:
    
    `git clone <project_url>`
    
    `cd <project_dir>`

- Create virtual environment: 

    `python -m venv venv`

- Switch the newly crated environment: 

    `source venv/bin/activate`
    
- Install dependencies:

    `pip install -r requirements.txt`
    
- Run tests:

    `pytest`

- Import project to the IDE. Set the project SDK to your virtual environment. Set test runner to `pytest`. 

## References

- Pytest - https://docs.pytest.org/en/latest/
- Selenium - https://selenium-python.readthedocs.io
- WebDriver Manager - https://github.com/SergeyPirogov/webdriver_manager
- Page Objects - https://martinfowler.com/bliki/PageObject.html
- Circular Imports - https://stackabuse.com/python-circular-imports


## See also

- Pytest Selenium - https://pytest-selenium.readthedocs.io/en/latest/
- SeleniumBase - http://seleniumbase.com
- pyPOM - https://pypom.readthedocs.io/en/latest/
- Faker - https://faker.readthedocs.io/en/master/
 