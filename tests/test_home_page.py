import pytest

from pages.home_page import HomePage


@pytest.fixture
def home_page(driver):
    return HomePage(driver)


def test_is_at(home_page):
    assert home_page.is_at()
    assert home_page.get_heading() == "Manage your contacts with ease!"
    assert home_page.get_sub_heading() == "Contacts App is a demo SPA application created in VueJS."


def test_navigates_to_login_page_and_back(home_page):
    login_page = home_page.navigate_to_login_page()
    assert login_page.is_at()
    assert login_page.get_page_title() == "Log in"

    home_page = login_page.cancel()
    assert home_page.is_at()


def test_navigates_to_signup_page_and_back(home_page):
    signup_page = home_page.navigate_to_signup_page()
    assert signup_page.is_at()
    assert signup_page.get_page_title() == "Signup"

    home_page = signup_page.cancel()
    assert home_page.is_at()


def test_navigates_to_contact_us_page_and_back(home_page):
    contact_us_page = home_page.navigate_to_contact_us_page()
    assert contact_us_page.is_at()
    assert contact_us_page.get_page_title() == "Contact Us"

    home_page = contact_us_page.cancel()
    assert home_page.is_at()


def test_opens_and_closes_about_dialog(home_page):
    dialog = home_page.open_about_dialog()
    assert dialog.is_at()
    assert dialog.get_dialog_title() == "About"

    dialog.close()
    assert home_page.is_at()


def test_opens_and_closes_login_dialog(home_page):
    dialog = home_page.open_login_dialog()
    assert dialog.is_at()
    assert dialog.get_dialog_title() == "Log in"

    dialog.close()
    assert home_page.is_at()


def test_opens_and_closes_signup_dialog(home_page):
    dialog = home_page.open_signup_dialog()
    assert dialog.is_at()
    assert dialog.get_dialog_title() == "Signup"

    dialog.close()
    assert home_page.is_at()
