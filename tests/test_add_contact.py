import pytest

from pages.contacts.contact import Contact
from pages.home_page import HomePage


@pytest.fixture
def contacts_page(driver):
    home_page = HomePage(driver)
    contacts_page = home_page.navigate_to_login_page().login("contacts", "demo")
    return contacts_page


def test_add_contact_with_name(contacts_page):
    form = contacts_page.open_contact_form_dialog()
    form.set_name("Joe Doe")
    contacts_page = form.submit()

    assert contacts_page.is_at()


def test_add_contact_with_name_and_email(contacts_page):
    form = contacts_page.open_contact_form_dialog()
    form.set_name("Jane Doe")
    form.set_email("jane.doe@example.com")
    contacts_page = form.submit()

    assert contacts_page.is_at()


def test_add_contact_with_name_2(contacts_page):
    contact = Contact("John Doe")

    form = contacts_page.open_contact_form_dialog()
    form.set_contact(contact)
    contacts_page = form.submit()

    assert contacts_page.is_at()
    assert contact in contacts_page.get_visible_contacts()


def test_add_contact_with_name_and_email_2(contacts_page):
    contact = Contact("John Doe", "john.doe@example.com")

    form = contacts_page.open_contact_form_dialog()
    form.set_contact(contact)
    contacts_page = form.submit()

    assert contacts_page.is_at()
    contacts = contacts_page.get_visible_contacts()
    assert contact in contacts
