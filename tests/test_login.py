import pytest

from pages.home_page import HomePage


@pytest.fixture
def home_page(driver):
    return HomePage(driver)


def test_logs_in_from_dialog_and_logs_out(home_page):
    login_dialog = home_page.open_login_dialog()
    contacts_page = login_dialog.login("contacts", "demo")

    assert contacts_page.is_at()
    assert contacts_page.get_page_title() == "Contacts"

    home_page = contacts_page.logout()
    assert home_page.is_at()


def test_logs_in_from_page_and_logs_out(home_page):
    login_page = home_page.navigate_to_login_page()
    contacts_page = login_page.login("contacts", "demo")

    assert contacts_page.is_at()
    assert contacts_page.get_page_title() == "Contacts"

    home_page = contacts_page.logout()
    assert home_page.is_at()


def test_logs_in_expecting_form_errors(home_page):
    login_page = home_page.navigate_to_login_page()
    errors = login_page.login_expecting_form_errors("x", "x")

    assert len(errors) == 2
    assert 'Username must be at least 3 characters long' in errors
    assert 'Password must be at least 3 characters long' in errors


def test_logs_in_expecting_username_error(home_page):
    login_page = home_page.navigate_to_login_page()
    errors = login_page.login_expecting_form_errors("x", "xxx")

    assert len(errors) == 1
    assert 'Username must be at least 3 characters long' in errors


def test_logs_in_expecting_password_error(home_page):
    login_page = home_page.navigate_to_login_page()
    errors = login_page.login_expecting_form_errors("xxx", "x")

    assert len(errors) == 1
    assert 'Password must be at least 3 characters long' in errors
